let nro =  Math.round(Math.random() * (2789999 - 2210000) + 2210000);

const nombre = "gaby";
const email = nro + '@yopmail.com';

describe('Test automation FE - todo.ly', function() {
  it('Sign UP', function(browser) {
    browser
    .navigateTo('https://todo.ly')

     .useXpath()
     .waitForElementVisible("(//*[contains(text(),'Get Organized')])")

     .useXpath()
     .click("(//div[3]/a/img)")
     .pause(1000)

     .useCss()
     .setValue('#ctl00_MainContent_SignupControl1_TextBoxFullName', 'gaby')
     .setValue('#ctl00_MainContent_SignupControl1_TextBoxEmail', email)
     .setValue('#ctl00_MainContent_SignupControl1_TextBoxPassword', 'control')
     .pause(1000)

     .useXpath()
     .click("(//input[@id='ctl00_MainContent_SignupControl1_CheckBoxTerms'])")
     .pause(1000)  
     
     .useXpath()
     .click("(//input[@id='ctl00_MainContent_SignupControl1_ButtonSignup'])")
     .pause(1000)
  }); 

  it('Select "Add new Project" ', function(browser) {
    browser

    .pause(1000)
    .useXpath ()
    .waitForElementVisible("(//div[@id='MainContentArea']/div[2])")
    .pause(1000)

    .useXpath()
    .click("(//table[@id='MainTable']/tbody/tr/td/div[6]/div)")
    .pause(1000)

  });

  it('Create new Project', function(browser) {
    browser
    /*
    .useXpath()
    .click("((//img[@title='Options'])[10])")
    .pause(1000) 
*/

    .useCss()
    .setValue('#NewProjNameInput', 'project'+nro)
    .pause(1000)

    .useXpath()
    .click("(//input[@id='NewProjNameButton'])")
    .pause(1000)  
    

    .useCss()
    .setValue('#NewItemContentInput', 'item'+nro)
    .pause(1000)

    .useXpath()
    .click("(//input[@id='NewItemAddButton'])")
    .pause(1000) 

  }); 

  it('Delete the project created', function(browser) {
    browser
    .pause(1000) 

    .useXpath()
    .click("(//li[6]/div )")
    .pause(1000) 

    .useXpath()
    .click("((//img[@title='Options'])[10])")
    .pause(1000) 

    .useXpath()
    .click("(//td/div/div/ul/li[4]/a)")
    .pause(1000) 

   // .click('@buttonPopup')
    .acceptAlert()

    .pause(3000) 

  }); 

});
