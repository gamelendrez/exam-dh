# Instrucciones
## Test automation FE:

### Descripcion 
Automatizacion de https://todo.ly/
1. Sign UP
2. Select "Add new Project"
3. Create new Project
4. Delete the project created

 ### Instrucciones
 1. Clonar el repositorio:
 ```ssh
 % git clone https://gitlab.com/<tu-Direccion>/exam-dh.git
 ```
2. Abrir la carpeta **DH** que esta en *testAutomationFE* desde VisualStudioCode.

 En la terminal, ejecutar el archivo:
  ```ssh
   npx nightwatch ./tests/todo.js
 ```

# Test automation BE: automatizar los siguientes endpoints

### Descripcion 

Automatizacion de http://todo.ly/ApiWiki/

1. Create User
2. Get User
3. Update User
4. Delete user

### Instrucciones
 1. Clonar el repositorio:
 ```ssh
 % git clone https://gitlab.com/<tu-Direccion>/exam-dh.git
 ```
2. Abrir la carpeta *testAutomationBE* 
3. Importar el archivo *DH.postman_collection.json* en Postman (para el caso se uso Postman Version 9.26.7)
4. Click el menu de la carpeta.
5. Seleccionar la opcion *'Run collection'* y la vista *'Run Settings'* es mostrada.
6. En el apartado *Data*, seleccionar el boton *Select file* y seleccione el archivo *email.csv* (mismo que se encuentra en la misma carpeta 'testAutomationBE')
7. Click sobre **Run DH**
